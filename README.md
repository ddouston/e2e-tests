# Overtone API E2E Tests

This repository contains a library of tests for Mastermind and Thanos. The goal is to provide coverage of each endpoint from the perspective of the API consumer.

## Requirements

[Nodejs](https://nodejs.org/en/) is required to run.

## Installation

Install project dependencies:

```bash
$ npm install
```

This installs [Frisbyjs](https://github.com/vlucas/frisby) and the [Jest](https://jestjs.io/en/) testrunner.

## Run Tests

Run Jest via NPM, and feed the appropriate jest config file:

```bash
$ npm run test -- --config env/jest.config.dev.json
```
