const frisby = require('frisby');
const joi = frisby.Joi;

const users = require('../../../test-data/users/dev.users');
const data = require('../../../test-data/post-bodies/login-post.json');

describe('Friends Test Suite', () => {

  it('should return user friend list', () => {

    user = users[0];
    data.parameters.userId = user.userId;
    data.parameters.password = user.password;

    // Log in with a user
    return frisby
      .post(`${global.baseUrl}/api/user/login`, data)
      .expect('status', 200)
      .then((res) => {

        let respObj = res.json;

        // Save the session token
        let sessionToken = respObj.results.session.token;
        let data = {
          option: 3
        }

        // Call Friends list api
        return frisby
          .setup({
            request: {
              headers: {
                'Authorization': `Bearer ${sessionToken}`
              }
            }
          })
          .post(`${global.baseUrl}/api/user/friends`, data)
          .expect('status', 200)
          .expect('jsonTypes', {
            'results': joi.object()
          })
          .expect('jsonTypes', {
            'results': {
              'friends': joi.array()
            }
          })
          .expect('jsonTypes', 'results.friends.*', {
            'friendId': joi.string(),
            'relationship': joi.number(),
            'userInitiated': joi.boolean()
          });

      });

  });

});