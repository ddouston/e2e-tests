const frisby = require('frisby');
const joi = frisby.Joi

describe('AppConfig Test Suite', () => {

  it('should return a response', () => {
    return frisby.get(`${global.baseUrl}/api/appconfig`)
      .expect('status', 200);
  });

});