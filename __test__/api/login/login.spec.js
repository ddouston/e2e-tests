const frisby = require('frisby');
const joi = frisby.Joi;

const users = require('../../../test-data/users/dev.users');
const data = require('../../../test-data/post-bodies/login-post.json');

describe('Login Test Suite', () => {

  it('should log in the user', () => {

    user = users[0];
    data.parameters.userId = user.userId;
    data.parameters.password = user.password;

    return frisby
      .post(`${global.baseUrl}/api/user/login`, data)
      .expect('status', 200)
      .then((res) => {
        let respObj = res.json;
        expect(respObj.results.session.username).toEqual(user.userId);
      });

  });

});